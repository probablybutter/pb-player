#ifndef PB_PLAYER_CAPTURE_CARD_HPP
#define PB_PLAYER_CAPTURE_CARD_HPP

//#include "pb_player/utils/com_raii.hpp"

#include "DeckLinkAPI.h"
#include "SDL.h"

#include <atomic>
#include <cstdint>
#include <chrono>

namespace pbp
{
  struct capture_card : public IDeckLinkInputCallback
  {
    ULONG m_refCount = 1;

    std::atomic_flag spinlock = ATOMIC_FLAG_INIT;

    IDeckLinkVideoInputFrame* curr_frame = nullptr;

    BMDDetectedVideoInputFormatFlags input_format_flags;
    IDeckLinkDisplayMode* display_mode = nullptr;
    BMDPixelFormat pixel_format = bmdFormat8BitYUV;
    IDeckLink* device = nullptr;
    IDeckLinkInput* recorder = nullptr;
    SDL_Window* main_window = nullptr;
    SDL_Renderer* renderer = nullptr;
    SDL_Texture* primary_tex = nullptr;

    SDL_AudioDeviceID audio_device = 0;
    SDL_AudioSpec audio_spec;
    int bytes_per_sample = 0;

    uint32_t format_changed_event_type;
    uint32_t video_frame_event_type;

    std::chrono::high_resolution_clock timer;

    ~capture_card();

    ///
    /// @brief start capturing
    ///
    void enable();

    ///
    /// @brief restart capture after video input configs updated
    ///
    void reinit();

    ///
    /// @brief stop capturing
    ///
    void disable();

    HRESULT STDMETHODCALLTYPE QueryInterface(REFIID iid, LPVOID* ppv) override;

    ULONG STDMETHODCALLTYPE AddRef(void) override;

    ULONG STDMETHODCALLTYPE Release(void) override;

    HRESULT STDMETHODCALLTYPE VideoInputFormatChanged(
      BMDVideoInputFormatChangedEvents change_type,
      IDeckLinkDisplayMode* new_display_mode,
      BMDDetectedVideoInputFormatFlags detected_signal_flags) override;

    HRESULT STDMETHODCALLTYPE VideoInputFrameArrived(
      IDeckLinkVideoInputFrame* video_frame,
      IDeckLinkAudioInputPacket* audio_frame) override;
  };
} // namespace pbp

#endif
