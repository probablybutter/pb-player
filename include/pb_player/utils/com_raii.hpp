#ifndef COM_RAII_HPP
#define COM_RAII_HPP

#include "raii_base.hpp"

#include "LinuxCOM.h"

namespace pbp
{
  template <class T>
  class com : private raii_base<T>
  {
    typedef raii_base<T> super_type;

  protected:
    void dec(T* p) override
    {
      if (p)
      {
        p->Release();
      }
    }

    void inc(T* p)
    {
      // TODO: should this check for nullptr?
      p->AddRef();
    }

  public:
    com() noexcept = default;

    com(T* d) noexcept : raii_base<T>(d)
    {
    }

    com(const com& o) : raii_base<T>(o.get())
    {
      inc(get());
    }

    ~com()
    {
      this->reset();
    }

    com& operator=(com&& o) noexcept
    {
      if (&o != this)
      {
        swap(o);
      }
      return *this;
    }

    com& operator=(const com& o) noexcept
    {
      if(&o != this)
      {
        raii_base<T>::operator=(o.get());
        inc(get());
      }
      return *this;
    }

    com& operator=(T* d) noexcept
    {
      raii_base<T>::operator=(d);
      return *this;
    }

    operator T*() noexcept
    {
      return reinterpret_cast<T*>(this->get());
    }

    operator const T*() const noexcept
    {
      return reinterpret_cast<const T*>(this->get());
    }

    T* operator->() noexcept
    {
      return get();
    }

    const T* operator->() const noexcept
    {
      return get();
    }

    using super_type::swap;
    using super_type::release;
    using super_type::operator*;
    using super_type::operator T*;
    using super_type::get;
    using super_type::reset;
  };
}

#endif
