#ifndef PBP_RAII_HPP
#define PBP_RAII_HPP

#include <utility>

namespace pbp
{
  ///
  /// @brief Resource acquisition is initialization wrapper
  /// This implementation is only for move-only types.
  /// Sub-classes should define an appropriate copy constructor for copyable types.
  ///
  template <class T, class PTR_TYPE = T*, class CPTR_TYPE = const T*>
  class raii_base
  {
    PTR_TYPE d_ = PTR_TYPE();

    raii_base(const raii_base&) = delete;
    raii_base& operator=(const raii_base&) = delete;

  protected:
    virtual void dec(PTR_TYPE d) = 0;

    raii_base& operator=(PTR_TYPE d)
    {
      dec(d_);
      d_ = d;
      return *this;
    }

    /**
     * @brief Child classes need to add reset();
     */
    ~raii_base() = default;

  public:
    raii_base() noexcept = default;

    raii_base(raii_base&& o) noexcept : d_(o.d_)
    {
      o.d_ = PTR_TYPE();
    }

    void swap(raii_base& o) noexcept
    {
      auto tmp = d_;
      d_ = o.d_;
      o.d_ = tmp;
    }

    raii_base(PTR_TYPE d) noexcept : d_(d)
    {
    }

    PTR_TYPE release()
    {
      auto res = d_;
      d_ = PTR_TYPE();
      return res;
    }

    PTR_TYPE& reset(PTR_TYPE p = PTR_TYPE())
    {
      dec(d_);
      d_ = p;
      return d_;
    }

    operator PTR_TYPE() const noexcept
    {
      return d_;
    }

    T& operator*()
    {
      return *d_;
    }

    const T& operator*() const
    {
      return *d_;
    }

    PTR_TYPE get() const noexcept
    {
      return d_;
    }
  };

  template <class T>
  class raii_base<T, T, T>
  {
    T d_ = T();

    raii_base(const raii_base&) = delete;
    raii_base& operator=(const raii_base&) = delete;

  protected:
    virtual void dec(T d) = 0;

    raii_base& operator=(T d)
    {
      dec(d_);
      d_ = d;
      return *this;
    }

    /**
     * @brief Child classes need to add reset();
     */
    ~raii_base() = default;

    template <class U>
    T release(U&& v)
    {
      auto res = d_;
      d_ = std::forward<U>(v);
      return res;
    }

  public:
    raii_base() noexcept = default;

    raii_base(raii_base&& o) noexcept : d_(o.d_)
    {
      o.d_ = T();
    }

    void swap(raii_base& o) noexcept
    {
      auto tmp = d_;
      d_ = o.d_;
      o.d_ = tmp;
    }

    raii_base(T d) noexcept : d_(d)
    {
    }

    T release()
    {
      auto res = d_;
      d_ = T();
      return res;
    }

    T& reset(T p = T())
    {
      dec(d_);
      d_ = p;
      return d_;
    }

    operator T() noexcept
    {
      return d_;
    }

    operator const T() const noexcept
    {
      return d_;
    }

    T& operator*()
    {
      return d_;
    }

    const T& operator*() const
    {
      return d_;
    }

    T get() noexcept
    {
      return d_;
    }

    const T get() const noexcept
    {
      return d_;
    }
  };
}
#endif
