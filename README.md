# About

PB Player is a simple interface program to the BlackMagic Decklink capture cards.
It just shows a video feed and plays sound.

Note that I use quite a few hard-coded values specific to my setup.
At some point I might generalize this to make it more useful like reading from a config file, but right now there are a few configuration settings which can be changed by hand:

- There is a "stretch" widescreen mode intended for use with an NTSC input stream formatted for widescreen output. This doesn't actually change the capture resolution, but only scales it. This mode only works with the NTSC format. The NTSC format uses a YUV422 8-bit color format using the Rec601 standard.
- The 1080p and 720p formats assume 60FPS (not 59.94). It's input as a 10-bit RGB 4:4:4 color format, but only the top 8 bits of each color are kept. Note that these are kind of finicky, and currently are quite CPU intensive.
- Audio is sampled at 16-bits, 48kHz stereo.

I don't really expect other people to have the same specific setup I do, but maybe other people can learn something interesting from the code to use in their projects. Sorry that the code is quite badly documented.

# Install

See [this page](doc/install.md) for build/install instructions.

# License

PB Player is licensed under the [MIT License](LICENSE.md).

Note that this program makes use of many different 3rd party libraries, in particular
the BlackMagic Decklink SDK (included in the deps folder), which I believe is licensed under the MIT license(?). See the individual libraries to see how they're licensed. 

