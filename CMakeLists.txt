cmake_minimum_required(VERSION 3.16)
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake/")

# lookup project version from git
include(GetGitRevisionDescription)
git_describe(PB_PLAYER_VERSION "--always")
string(REGEX REPLACE "(-|\\.)" ";" VER_TMP ${PB_PLAYER_VERSION})
list(GET VER_TMP 0 PB_PLAYER_VERSION_MAJOR)
list(GET VER_TMP 1 PB_PLAYER_VERSION_MINOR)
list(GET VER_TMP 2 PB_PLAYER_VERSION_PATCH)
unset(VER_TMP)

project(pb_player VERSION "${PB_PLAYER_VERSION_MAJOR}.${PB_PLAYER_VERSION_MINOR}.${PB_PLAYER_VERSION_PATCH}")

include(CMakeDependentOption)

include(GNUInstallDirs)
set(INSTALL_CONFIGDIR ${CMAKE_INSTALL_DATAROOTDIR}/cmake/pb_player)

# build flags
if(NOT CMAKE_BUILD_TYPE)
  message(STATUS "Setting build type to 'Release' as none was specified.")
  set(CMAKE_BUILD_TYPE Release CACHE STRING "Choose the type of build." FORCE)
endif()

if(NOT CMAKE_CONFIGURATION_TYPES)
  # Set the possible values of build type for cmake-gui
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release" "MinSizeRel" "RelWithDebInfo")
endif()

# use ld.lld linker if available
option(USE_LLD "Use LLD Linker, if available" OFF)
option(USE_LTO "Use Link-Time Optimizations" OFF)

set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -march=native")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -march=native")

if (USE_LLD)
  execute_process(
	  COMMAND ${CMAKE_C_COMPILER} -fuse-ld=lld -Wl,--version
	  ERROR_QUIET OUTPUT_VARIABLE LD_VERSION)
  if ("${LD_VERSION}" MATCHES "LLD")
	  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fuse-ld=lld")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -fuse-ld=lld")
    set(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} -fuse-ld=lld")
    message(STATUS "Using LLD linker")
  else()
    message(ERROR "Couldn't find LLD Linker, using falling back to system linker")
  endif()
  unset(LD_VERSION)
endif()

if(USE_LTO)
  if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang" OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "AppleClang")
    if(USE_LLD)
      # Enable ThinLTO if it's possible to do so
      message(STATUS "Enabling ThinLTO for Release")
      set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -flto=thin")
      set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -flto=thin")
    endif()
  elseif("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
    message(STATUS "Enabling IPO for Release")
    set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -fipo")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -fipo")
  else()
    message(STATUS "Enabling LTO for Release")
    set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -flto")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -flto")
  endif()
endif()

##
# @brief Symbolically links a list of files to build directory
# @param ARGN files to link
function(SYMLINK_FILES)
    # make absolute paths
    get_property(is_defined GLOBAL PROPERTY PB_PLAYER_SYM_SRCS DEFINED)
    if(NOT is_defined)
        define_property(GLOBAL PROPERTY PB_PLAYER_SYM_SRCS
            BRIEF_DOCS "List of all symlink files to link to build directory"
            FULL_DOCS "List of all symlink files to link to build directory")
    endif()

    get_property(is_defined GLOBAL PROPERTY PB_PLAYER_SYM_DSTS DEFINED)
    if(NOT is_defined)
        define_property(GLOBAL PROPERTY PB_PLAYER_SYM_DSTS
            BRIEF_DOCS "List of all symlink files to link to build directory"
            FULL_DOCS "List of all symlink files to link to build directory")
    endif()
    
    set(SRCS)
    set(DSTS)
    foreach(s IN LISTS ARGN)
        list(APPEND DSTS "${CMAKE_CURRENT_BINARY_DIR}/${s}")
        if(NOT IS_ABSOLUTE "${s}")
            get_filename_component(s "${s}" ABSOLUTE)
        endif()
        list(APPEND SRCS "${s}")
    endforeach()
    set_property(GLOBAL APPEND PROPERTY PB_PLAYER_SYM_SRCS "${SRCS}")
    set_property(GLOBAL APPEND PROPERTY PB_PLAYER_SYM_DSTS "${DSTS}")
endfunction()

# add user modules
include("pb_player-modules")

configure_file(
  "${PROJECT_SOURCE_DIR}/pb_player_config.hpp.in"
  "${PROJECT_BINARY_DIR}/include/pb_player/pb_player_config.hpp"
  )

configure_file(
  "${PROJECT_SOURCE_DIR}/pb_player_version.hpp.in"
  "${PROJECT_BINARY_DIR}/include/pb_player/pb_player_version.hpp"
  )

add_subdirectory("deps/blackmagic")

# user source code
add_subdirectory(src)

add_subdirectory(tools)

# documentation
include("pb_player-doc")

# install TODO

# symlinks
function(GENERATE_SYM_LINKS)
    message(STATUS "Generating symbolic links")
    get_property(SRCS GLOBAL PROPERTY PB_PLAYER_SYM_SRCS)
    get_property(DSTS GLOBAL PROPERTY PB_PLAYER_SYM_DSTS)
    list(LENGTH SRCS len1)
    if(len1)
        math(EXPR len2 "${len1}-1")
        foreach(var RANGE ${len2})
            list(GET SRCS ${var} src)
            list(GET DSTS ${var} dst)
            add_custom_command(OUTPUT "${dst}"
                COMMAND ln -sf "${src}" "${dst}"
                DEPENDS "${src}"
                )
            set_source_files_properties("${dst}"
                PROPERTIES GENERATED TRUE)
        endforeach()
        add_custom_target(PB_PLAYER_SYM_LINKS ALL
            DEPENDS ${DSTS})
    endif()
endfunction()

GENERATE_SYM_LINKS()
