# Install instructions

## Building from source

### Dependencies

- C++14 compatible compiler
- Linux operating system (I use Ubuntu, but should work on other variants provided you have the required libraries)
- [CMake 3.1+](http://www.cmake.org/)
- [Git](https://git-scm.com/)
- [pkg-config](https://www.freedesktop.org/wiki/Software/pkg-config/)
- [cairo](https://www.cairographics.org/)
- [GTK+3.16 or newer](https://www.gtk.org/)
- [glib-2.0](https://github.com/GNOME/glib)
- [Pulse Audio API](https://freedesktop.org/software/pulseaudio/doxygen/) (libpulse, and libpulse-simple)
- [optional] [Doxygen](http://www.stack.nl/~dimitri/doxygen/): for building the documentation.
- PThreads
- [Blackmagic Decklink driver](https://www.blackmagicdesign.com/support/family/capture-and-playback): I'm using the 10.11.2 driver, and the included SDK is associated with v10.11.2. It probably works if you use a different driver version, but I haven't tested it.
- [Boost](https://www.boost.org/) I think currently the only required component is StringView?

### Basic Building

It is highly recommended to [build out-of-source](http://voices.canonical.com/jussi.pakkanen/2013/04/16/why-you-should-consider-using-separate-build-directories/). This project uses CMake to configure. Important parameters:

- `CMAKE_BUILD_TYPE`: Which build configuration to use (Release, Debug, etc.). Defaults to Debug build.
- `CMAKE_CXX_FLAGS`: Additional C++ compiler flags
- `BUILD_DOCUMENTATION`: Whether doxygen documentation should be built
- `PB_PLAYER_INTERNAL_DOCS`: Build internal docs useful for developers.

Example configuration for Unix:

~~~{.sh}
# Assuming root project directory is PROJ_ROOT:
mkdir ${PROJ_ROOT}/../build
cd ${PROJ_ROOT}/../build
cmake ${PROJ_ROOT}
make
~~~

Additional info on using CMake can be found on their [Wiki](http://www.cmake.org/Wiki/CMake).

