# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

#[=======================================================================[.rst:
FindSDL2_ttf
-------

Locate the SDL2 Ttf library


Imported targets
^^^^^^^^^^^^^^^^

This module defines the following :prop_tgt:`IMPORTED` target:

``SDL2::SDL2_ttf``
  The SDL library, if found

Result variables
^^^^^^^^^^^^^^^^

This module will set the following variables in your project:

``SDL2_TTF_INCLUDE_DIRS``
  where to find SDL_ttf.h
``SDL2_TTF_LIBRARIES``
  the name of the library to link against
``SDL2_ttf_FOUND``
  if false, do not try to link to SDL
``SDL2_TTF_VERSION``
  the human-readable string containing the version of SDL if found
``SDL2_TTF_VERSION_MAJOR``
  SDL2 ttf major version
``SDL2_TTF_VERSION_MINOR``
  SDL2 ttf minor version
``SDL2_TTF_VERSION_PATCH``
  SDL2 ttf patch version

Cache variables
^^^^^^^^^^^^^^^

These variables may optionally be set to help this module find the correct files:

``SDL2_TTF_INCLUDE_DIR``
  where to find SDL_ttf.h
``SDL2_TTF_LIBRARY``
  the name of the library to link against


Obsolete variables
^^^^^^^^^^^^^^^^^^

These variables are obsolete and provided for backwards compatibility:

``SDL2_TTF_VERSION_STRING``
  the human-readable string containing the version of SDL2 Ttf if found.
  Identical to SDL2_TTF_VERSION


Additional Note: If you see an empty SDL2_TTF_LIBRARY_TEMP in your
configuration and no SDL2_TTF_LIBRARY, it means CMake did not find your SDL2 ttf
library (SDL2_ttf.dll, libsdl2_ttf.so, SDL2_ttf.framework, etc).  Set
SDL2_TTF_LIBRARY_TEMP to point to your SDL2 library, and configure again.
These values are used to generate the final
SDL2_TTF_LIBRARY variable, but when these values are unset, SDL2_TTF_LIBRARY
does not get created.

$SDL2TTFDIR is an environment variable that would correspond to the
./configure --prefix=$SDL2TTFDIR used in building SDL2.  l.e.galup 9-20-02

On OSX, this will prefer the Framework version (if found) over others.
People will have to manually change the cache values of SDL2_TTF_LIBRARY to
override this selection or set the CMake environment
CMAKE_INCLUDE_PATH to modify the search paths.
#]=======================================================================]

find_path(SDL2_TTF_INCLUDE_DIR
  NAMES SDL_ttf.h
  HINTS
    ENV SDL2TTFDIR
  PATH_SUFFIXES
  # path suffixes to search inside ENV{SDLDIR}
  include/SDL2 include
)

if(CMAKE_SIZEOF_VOID_P EQUAL 8)
  set(VC_LIB_PATH_SUFFIX lib/x64)
else()
  set(VC_LIB_PATH_SUFFIX lib/x86)
endif()

find_library(SDL2_TTF_LIBRARY_TEMP
  NAMES SDL2_ttf
  HINTS
    ENV SDL2TTFDIR
  PATH_SUFFIXES lib ${VC_LIB_PATH_SUFFIX}
)

# Hide this cache variable from the user, it's an internal implementation
# detail. The documented library variable for the user is SDL2_LIBRARY
# which is derived from SDL2_LIBRARY_TEMP further below.
set_property(CACHE SDL2_TTF_LIBRARY_TEMP PROPERTY TYPE INTERNAL)

# SDL2 may require threads on your system.
# The Apple build may not need an explicit flag because one of the
# frameworks may already provide it.
# But for non-OSX systems, I will use the CMake Threads package.
if(NOT APPLE)
  find_package(Threads)
endif()

# MinGW needs an additional link flag, -mwindows
# It's total link flags should look like -lmingw32 -lSDL2main -lSDL2 -mwindows
if(MINGW)
  set(MINGW32_LIBRARY mingw32 "-mwindows" CACHE STRING "link flags for MinGW")
endif()

if(SDL2_TTF_LIBRARY_TEMP)
  # For OS X, SDL2 uses Cocoa as a backend so it must link to Cocoa.
  # CMake doesn't display the -framework Cocoa string in the UI even
  # though it actually is there if I modify a pre-used variable.
  # I think it has something to do with the CACHE STRING.
  # So I use a temporary variable until the end so I can set the
  # "real" variable in one-shot.
  if(APPLE)
    set(SDL2_TTF_LIBRARY_TEMP ${SDL2_TTF_LIBRARY_TEMP} "-framework Cocoa")
  endif()

  # For threads, as mentioned Apple doesn't need this.
  # In fact, there seems to be a problem if I used the Threads package
  # and try using this line, so I'm just skipping it entirely for OS X.
  if(NOT APPLE)
    set(SDL2_TTF_LIBRARY_TEMP ${SDL2_TTF_LIBRARY_TEMP} ${CMAKE_THREAD_LIBS_INIT})
  endif()

  # For MinGW library
  if(MINGW)
    set(SDL2_TTF_LIBRARY_TEMP ${MINGW32_LIBRARY} ${SDL2_TTF_LIBRARY_TEMP})
  endif()

  # Set the final string here so the GUI reflects the final state.
  set(SDL2_TTF_LIBRARY ${SDL2_TTF_LIBRARY_TEMP} CACHE STRING "Where the SDL2 Ttf Library can be found")
endif()

if(SDL2_TTF_INCLUDE_DIR AND EXISTS "${SDL2_TTF_INCLUDE_DIR}/SDL_ttf.h")
  file(STRINGS "${SDL2_TTF_INCLUDE_DIR}/SDL_ttf.h" SDL2_TTF_VERSION_MAJOR_LINE REGEX "^#define[ \t]+SDL_TTF_MAJOR_VERSION[ \t]+[0-9]+$")
  file(STRINGS "${SDL2_TTF_INCLUDE_DIR}/SDL_ttf.h" SDL2_TTF_VERSION_MINOR_LINE REGEX "^#define[ \t]+SDL_TTF_MINOR_VERSION[ \t]+[0-9]+$")
  file(STRINGS "${SDL2_TTF_INCLUDE_DIR}/SDL_ttf.h" SDL2_TTF_VERSION_PATCH_LINE REGEX "^#define[ \t]+SDL_TTF_PATCHLEVEL[ \t]+[0-9]+$")
  string(REGEX REPLACE "^#define[ \t]+SDL_TTF_MAJOR_VERSION[ \t]+([0-9]+)$" "\\1" SDL2_TTF_VERSION_MAJOR "${SDL2_TTF_VERSION_MAJOR_LINE}")
  string(REGEX REPLACE "^#define[ \t]+SDL_TTF_MINOR_VERSION[ \t]+([0-9]+)$" "\\1" SDL2_TTF_VERSION_MINOR "${SDL2_TTF_VERSION_MINOR_LINE}")
  string(REGEX REPLACE "^#define[ \t]+SDL_TTF_PATCHLEVEL[ \t]+([0-9]+)$" "\\1" SDL2_TTF_VERSION_PATCH "${SDL2_TTF_VERSION_PATCH_LINE}")
  unset(SDL2_TTF_VERSION_MAJOR_LINE)
  unset(SDL2_TTF_VERSION_MINOR_LINE)
  unset(SDL2_TTF_VERSION_PATCH_LINE)
  set(SDL2_TTF_VERSION ${SDL2_TTF_VERSION_MAJOR}.${SDL2_TTF_VERSION_MINOR}.${SDL2_TTF_VERSION_PATCH})
  set(SDL2_TTF_VERSION_STRING ${SDL2_TTF_VERSION})
endif()

include(FindPackageHandleStandardArgs)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(SDL2_ttf
                                  REQUIRED_VARS SDL2_TTF_LIBRARY SDL2_TTF_INCLUDE_DIR
                                  VERSION_VAR SDL2_TTF_VERSION_STRING)

if(SDL2_ttf_FOUND)
  set(SDL2_TTF_LIBRARIES ${SDL2_TTF_LIBRARY})
  set(SDL2_TTF_INCLUDE_DIRS ${SDL2_TTF_INCLUDE_DIR})
  if(NOT TARGET SDL2::SDL2_ttf)
    add_library(SDL2::SDL2_ttf INTERFACE IMPORTED)
    set_target_properties(SDL2::SDL2_ttf PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES "${SDL2_TTF_INCLUDE_DIRS}"
      INTERFACE_LINK_LIBRARIES "${SDL2_TTF_LIBRARY}")
  endif()
endif()
