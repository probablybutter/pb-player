#include "pb_player/capture_card.hpp"
#include "pb_player/pb_player_version.hpp"

#include "SDL.h"
#include <iostream>
#include <cstdlib>
#include <getopt.h>
#include <cstring>
#include <chrono>

void print_help(bool detailed)
{
  std::clog << "Usage: pb_player";
  std::clog << " [-t TRIM_X:TRIM_Y] [-s WINDOW_X:WINDOW_Y] [-s SCALE] [-h] "
               "[--trim=TRIM_X:TRIM_Y] [--size=WINDOW_X:WINDOW_Y] "
               "[--size=SCALE] [--help]\n";

  if (detailed)
  {
    std::clog
      << "\npb_player displays video output from Blackmagic capture cards\n\n";
    std::clog << "version " PB_PLAYER_VERSION " ProbablyButter (c) 2020\n\n";

    std::clog << "    -t TRIM_X:TRIM_Y, --trim=TRIM_X:TRIM_Y\n"
              << "        how much to trim off the edges.\n"
              << "        Default: 8:3\n\n";

    std::clog << "    -s WINDOW_X:WINDOW_Y, --size=WINDOW_X:WINDOW_Y\n"
              << "        The window size.\n"
              << "        Default: 1280:960\n\n";
    std::clog << "    -s SCALE, --size=SCALE\n"
              << "        How much to scale the window size from the capture "
                 "resolution.\n"
              << "        Default: 0\n\n";

    std::clog << "    -h, --help\n"
              << "        Prints this help and exits.\n";
  }
}

std::string mode_name(IDeckLinkDisplayMode* display_mode)
{
  HRESULT err;
  const char* display_mode_string;
  err = display_mode->GetName(&display_mode_string);
  if (err != S_OK)
  {
    return "";
  }
  std::string res = display_mode_string;
  free(reinterpret_cast<void*>(const_cast<char*>(display_mode_string)));
  return res;
}

std::string format_pixel_format(BMDPixelFormat pixel_format)
{
  switch (pixel_format)
  {
  case bmdFormatUnspecified:
  default:
    return "unspecified";
  case bmdFormat8BitYUV:
    return "8-bit YUV";
  case bmdFormat10BitYUV:
    return "10-bit YUV";
  case bmdFormat8BitARGB:
    return "8-bit ARGB";
  case bmdFormat8BitBGRA:
    return "8-bit BGRA";
  case bmdFormat10BitRGB:
    return "10-bit RGB";
  case bmdFormat12BitRGB:
    return "12-bit RGB";
  case bmdFormat12BitRGBLE:
    return "12-bit RGBLE";
  case bmdFormat10BitRGBXLE:
    return "10-bit RGBXLE";
  case bmdFormat10BitRGBX:
    return "10-bit RGBX";
  }
}

int event_filter(void* userdata, SDL_Event* event)
{
  pbp::capture_card* card = (pbp::capture_card*)userdata;
  return event->type == card->video_frame_event_type ||
         event->type == SDL_QUIT ||
         event->type == card->format_changed_event_type;
}

int main(int argc, char** argv)
{
  // parse command-line
  // scale of 0 means fixed window size
  double scale = 0;
  int format = 0;

  // starting assumptions: what's good for MM
  int window_width = 640 * 2;
  int window_height = 480 * 2;
  SDL_Rect tex_draw_rect;
  // mm settings: trim_x = 8, trim_y = 3
  tex_draw_rect.x = 8;
  tex_draw_rect.y = 3;
  tex_draw_rect.w = 720;
  tex_draw_rect.h = 486;

  auto pixel_format = SDL_PIXELFORMAT_UYVY;
  {
    static struct option long_options[] = {
      {"trim", required_argument, nullptr, 't'},
      {"size", required_argument, nullptr, 's'},
      {"help", no_argument, nullptr, 0}, {nullptr, 0, nullptr, 0}};
    int c;
    int idxptr;
    while ((c = getopt_long(argc, argv, "t:s:h", long_options, &idxptr)) != -1)
    {
      switch (c)
      {
      case 's': {
        int i = 0;
        // look for ':'
        while (optarg[i])
        {
          if (optarg[i] == ':')
          {
            if (i == 0)
            {
              // bad format
              std::cerr << "bad size input\n";
              print_help(false);
              return -1;
            }
            break;
          }
          ++i;
        }
        if (optarg[i])
        {
          // specified x:y
          window_width = atoll(optarg);
          window_height = atoll(optarg + i + 1);
          scale = 0;
        }
        else
        {
          scale = atof(optarg);
        }
      }
      break;
      case 't': {
        int i = 0;
        // look for ':'
        while (optarg[i])
        {
          if (optarg[i] == ':')
          {
            if (i == 0)
            {
              // bad format
              std::cerr << "bad trim input\n";
              print_help(false);
              return -1;
            }
            break;
          }
          ++i;
        }
        if (optarg[i])
        {
          tex_draw_rect.x = atoll(optarg);
          tex_draw_rect.y = atoll(optarg + i + 1);
        }
        else
        {
          std::cerr << "bad trim input\n";
          print_help(false);
          return -1;
        }
      }
      break;
      case 'h':
        // quick help
        print_help(false);
        return -1;
      case 0:
        // detailed help
        print_help(true);
        return -1;
      default:
        break;
      }
    }
  }

  // initialize SDL
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0)
  {
    std::cerr << "Unable to initialize SDL: " << SDL_GetError() << std::endl;
    return 1;
  }
  // setup exit handler
  atexit(SDL_Quit);

  SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "nearest");

  pbp::capture_card* card = new pbp::capture_card();
  auto last_frame = card->timer.now();

  card->format_changed_event_type = SDL_RegisterEvents(1);
  card->video_frame_event_type = SDL_RegisterEvents(1);
  // filter all mouse and keyboard events
  SDL_SetEventFilter(event_filter, (void*)card);
  // TODO: setup capture card parameters
  IDeckLinkIterator* iter = CreateDeckLinkIteratorInstance();
  if (!iter)
  {
    std::cerr << "no blacklink driver\n";
    goto terminate;
  }
  // use first device found
  if (iter->Next(&card->device) != S_OK)
  {
    std::cerr << "no blacklink devices found\n";
    goto terminate;
  }

  // get the capture interface
  if (card->device->QueryInterface(
        IID_IDeckLinkInput, (void**)&card->recorder) != S_OK)
  {
    std::cerr << "Couldn't get capture interface";
    goto terminate;
  }

  card->recorder->SetCallback(card);

  // setup Audio interface
  {
    SDL_AudioSpec desired_audio;
    // TODO: how to detect these?
    // pretty sure only 48kHz is supported by Blackmagic
    desired_audio.freq = 48000;
    desired_audio.format = AUDIO_S16;
    desired_audio.channels = 2;
    desired_audio.samples = 0;
    desired_audio.callback = nullptr;
    desired_audio.userdata = nullptr;
    const char* audio_driver = SDL_GetCurrentAudioDriver();
    if (audio_driver)
    {
      card->audio_device = SDL_OpenAudioDevice(nullptr, 0, &desired_audio,
        &card->audio_spec, SDL_AUDIO_ALLOW_ANY_CHANGE);
      if (card->audio_device <= 0)
      {
        std::cerr << "Failure initializing audio: " << SDL_GetError()
                  << std::endl;
        goto terminate;
      }
      else
      {
        // figure out how many bytes per sample there are
        switch (card->audio_spec.format)
        {
        case AUDIO_S8:
        case AUDIO_U8:
          card->bytes_per_sample = 1;
          break;
        case AUDIO_S16LSB:
        case AUDIO_S16MSB:
        case AUDIO_U16LSB:
        case AUDIO_U16MSB:
          card->bytes_per_sample = 2;
          break;
        case AUDIO_S32LSB:
        case AUDIO_S32MSB:
          card->bytes_per_sample = 4;
          break;
        }
        // need to unpause
        SDL_PauseAudioDevice(card->audio_device, 0);
      }
    }
    else
    {
      std::cout << "No audio driver\n";
      goto terminate;
    }
  }

  // get initial display mode
  {
    IDeckLinkDisplayModeIterator* dmode_iter = nullptr;
    card->recorder->GetDisplayModeIterator(&dmode_iter);
    while (dmode_iter->Next(&card->display_mode) == S_OK)
    {
      std::cout << "using display mode " << mode_name(card->display_mode)
                << ", " << format_pixel_format(card->pixel_format) << "\n";
      tex_draw_rect.w = card->display_mode->GetWidth() - tex_draw_rect.x * 2;
      tex_draw_rect.h = card->display_mode->GetHeight() - tex_draw_rect.y * 2;
      if (scale)
      {
        window_width = tex_draw_rect.w * scale;
        window_height = tex_draw_rect.h * scale;
      }
      break;
    }
    dmode_iter->Release();
  }

#if 0
  card->main_window = SDL_CreateWindow("PB Player", SDL_WINDOWPOS_UNDEFINED,
    SDL_WINDOWPOS_UNDEFINED, window_width, window_height,
    SDL_WINDOW_BORDERLESS);
#else
  card->main_window = SDL_CreateWindow("PB Player", SDL_WINDOWPOS_UNDEFINED,
    SDL_WINDOWPOS_UNDEFINED, window_width, window_height, SDL_WINDOW_OPENGL | SDL_WINDOW_BORDERLESS);
#endif

  card->renderer =
    SDL_CreateRenderer(card->main_window, -1, SDL_RENDERER_ACCELERATED);

  // setup texture for streaming data
  card->primary_tex =
    SDL_CreateTexture(card->renderer, pixel_format, SDL_TEXTUREACCESS_STREAMING,
      card->display_mode->GetWidth(), card->display_mode->GetHeight());

  // start capturing
  card->enable();

  // main event loop
  while (true)
  {
    SDL_Event e;
    // handle events
    //while (SDL_WaitEventTimeout(&e, 8))
    while (SDL_PollEvent(&e))
    {
      if (e.type == SDL_QUIT)
      {
        goto quit;
      }
      else if (e.type == card->format_changed_event_type)
      {
        // updated display mode
        while (card->spinlock.test_and_set(std::memory_order_acquire))
          ;
        if (card->input_format_flags & bmdDetectedVideoInputRGB444)
        {
          // switch to an RGB444-based texture
          pixel_format = SDL_PIXELFORMAT_ARGB4444;
          card->pixel_format = bmdFormat8BitARGB;
        }
        else if (card->input_format_flags & bmdDetectedVideoInputYCbCr422)
        {
          // switch to a YUV422-based texture
          pixel_format = SDL_PIXELFORMAT_UYVY;
          card->pixel_format = bmdFormat8BitYUV;
        }
        if (card->curr_frame)
        {
          // current frame likely isn't valid
          card->curr_frame->Release();
          card->curr_frame = nullptr;
        }
        std::cout << "switching to display mode "
                  << mode_name(card->display_mode) << ", "
                  << format_pixel_format(card->pixel_format) << "\n";
        card->reinit();
        // create a new texture
        SDL_DestroyTexture(card->primary_tex);
        card->primary_tex = SDL_CreateTexture(card->renderer, pixel_format,
          SDL_TEXTUREACCESS_STREAMING, card->display_mode->GetWidth(),
          card->display_mode->GetHeight());
        // update sizes
        tex_draw_rect.w = card->display_mode->GetWidth() - tex_draw_rect.x * 2;
        tex_draw_rect.h = card->display_mode->GetHeight() - tex_draw_rect.y * 2;
        if (scale)
        {
          window_width = tex_draw_rect.w * scale;
          window_height = tex_draw_rect.h * scale;
          SDL_SetWindowSize(card->main_window, window_width, window_height);
        }
        card->spinlock.clear(std::memory_order_release);
      }
      else if (e.type == card->video_frame_event_type)
      {
        {
          while (card->spinlock.test_and_set(std::memory_order_acquire))
            ;
          if (card->curr_frame)
          {
            // TODO: draw frame to texture
            unsigned char* frame_data;
            card->curr_frame->GetBytes((void**)&frame_data);
            auto width = card->curr_frame->GetRowBytes();
            auto height = card->curr_frame->GetHeight();
            unsigned char* tex_data;
            int pitch;
            SDL_LockTexture(
              card->primary_tex, nullptr, (void**)&tex_data, &pitch);
#ifdef VERIFY
            if (pitch == width && card->display_mode->GetHeight() == height)
#endif
            {
              // likely a valid frame
              std::copy(frame_data, frame_data + width * height, tex_data);
            }
#ifdef VERIFY
            else
            {
              // TODO: zero-out tex_data
              std::cout << "invalid frame\n";
            }
#endif
            SDL_UnlockTexture(card->primary_tex);
            // cleanup
            card->curr_frame->Release();
            card->curr_frame = nullptr;
          }
          card->spinlock.clear(std::memory_order_release);
        }
        // is skipping due to receiving multiple frames in one pass?
        SDL_RenderCopy(
          card->renderer, card->primary_tex, &tex_draw_rect, nullptr);
        SDL_RenderPresent(card->renderer);
#if 0
        auto draw_time = card->timer.now();
        if (std::chrono::duration_cast<std::chrono::nanoseconds>(
              draw_time - last_frame)
                .count() *
              1e-9 >=
            0.035)
        {
          std::cerr << "frame(s) skipped: "
                    << std::chrono::duration_cast<std::chrono::nanoseconds>(
                         draw_time - last_frame)
                           .count() *
                         1e-6
                    << " ms\n";
        }
        last_frame = draw_time;
#endif
      }
    }

    // clearing isn't necessary since we're drawing over everything
    // SDL_RenderClear(renderer);
    // SDL_RenderCopy(card->renderer, card->primary_tex, &tex_draw_rect, nullptr);
    // SDL_RenderPresent(card->renderer);
  }
quit:

  // cleanup
  // stop capturing
  card->disable();
  // how do I ensure safe thread joining? for now, just wait a bit
  SDL_Delay(50);
terminate:
  if (iter)
  {
    iter->Release();
  }
  delete card;

  return 0;
}
