#include "pb_player/capture_card.hpp"

#include <iostream>

namespace pbp
{
  capture_card::~capture_card()
  {
    if (curr_frame)
    {
      curr_frame->Release();
    }
    if (display_mode)
    {
      display_mode->Release();
    }
    if (device)
    {
      device->Release();
    }
    if (recorder)
    {
      recorder->Release();
    }

    if (primary_tex)
    {
      SDL_DestroyTexture(primary_tex);
    }
    if (renderer)
    {
      SDL_DestroyRenderer(renderer);
    }
    if (main_window)
    {
      SDL_DestroyWindow(main_window);
    }
  }

  void capture_card::enable()
  {
    recorder->EnableVideoInput(display_mode->GetDisplayMode(), pixel_format,
      bmdVideoInputEnableFormatDetection);
    recorder->EnableAudioInput(
      bmdAudioSampleRate48kHz, bytes_per_sample * 8, audio_spec.channels);
    recorder->StartStreams();
  }

  void capture_card::reinit()
  {
    recorder->PauseStreams();
    auto err = recorder->EnableVideoInput(display_mode->GetDisplayMode(),
      pixel_format, bmdVideoInputEnableFormatDetection);
    if (err != S_OK)
    {
      std::cout << "Failed to switch video mode\n";
    }
    recorder->FlushStreams();
    recorder->StartStreams();
  }

  void capture_card::disable()
  {
    recorder->StopStreams();
    recorder->DisableAudioInput();
    recorder->DisableVideoInput();
  }

  HRESULT STDMETHODCALLTYPE capture_card::QueryInterface(
    REFIID iid, LPVOID* ppv)
  {
    return E_NOINTERFACE;
  }

  ULONG STDMETHODCALLTYPE capture_card::AddRef(void)
  {
    return __sync_add_and_fetch(&m_refCount, 1);
  }

  ULONG STDMETHODCALLTYPE capture_card::Release(void)
  {
    ULONG newRefValue = __sync_sub_and_fetch(&m_refCount, 1);
    if (newRefValue == 0)
    {
      delete this;
      return 0;
    }
    return newRefValue;
  }

  HRESULT STDMETHODCALLTYPE capture_card::VideoInputFormatChanged(
    BMDVideoInputFormatChangedEvents change_type,
    IDeckLinkDisplayMode* new_display_mode,
    BMDDetectedVideoInputFormatFlags detected_signal_flags)
  {
    // TODO: can this ever be null?
    if (new_display_mode)
    {
      new_display_mode->AddRef();
    }
    SDL_Event event;
    event.type = format_changed_event_type;
    {
      while (spinlock.test_and_set(std::memory_order_acquire))
        ;
      input_format_flags = detected_signal_flags;
      if (new_display_mode)
      {
        if (display_mode)
        {
          display_mode->Release();
        }
        display_mode = new_display_mode;
      }
      spinlock.clear(std::memory_order_release);
    }
    // fire event notifying of video input format change
    SDL_PushEvent(&event);
    return S_OK;
  }

  HRESULT STDMETHODCALLTYPE capture_card::VideoInputFrameArrived(
    IDeckLinkVideoInputFrame* video_frame,
    IDeckLinkAudioInputPacket* audio_frame)
  {
    auto start = timer.now();
    if (video_frame)
    {
      video_frame->AddRef();
      {
        while (spinlock.test_and_set(std::memory_order_acquire))
          ;
        if (curr_frame)
        {
          curr_frame->Release();
        }
        curr_frame = video_frame;
        spinlock.clear(std::memory_order_release);
      }
      // trigger a video frame update
      SDL_Event event;
      event.type = video_frame_event_type;
      SDL_PushEvent(&event);
    }
    auto vframe_end = timer.now();
    if (audio_frame)
    {
      // play audio
      void* audio_data;
      audio_frame->GetBytes(&audio_data);
      auto frame_count = audio_frame->GetSampleFrameCount();
      auto err = SDL_QueueAudio(audio_device, audio_data,
        frame_count * audio_spec.channels * bytes_per_sample);
      if (err)
      {
        std::cerr << SDL_GetError() << "\n";
      }
    }
    auto stop = timer.now();
    if (std::chrono::duration_cast<std::chrono::nanoseconds>(stop - start)
            .count() *
          1e-9 >=
        .017)
    {
      std::cerr << "capture processing: "
                << std::chrono::duration_cast<std::chrono::nanoseconds>(
                     stop - vframe_end)
                       .count() *
                     1e-6
                << " ms, "
                << std::chrono::duration_cast<std::chrono::nanoseconds>(
                     vframe_end - start)
                       .count() *
                     1e-6
                << " ms\n";
    }

    return S_OK;
  }
} // namespace pbp
