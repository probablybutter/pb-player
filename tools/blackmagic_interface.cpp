#include <iostream>
#include <stdlib.h>
#include <getopt.h>
#include <cstring>
#include <chrono>
#include <vector>
#include <string>

#include "DeckLinkAPI.h"

std::string mode_name(IDeckLinkDisplayMode* display_mode)
{
  HRESULT err;
  const char* display_mode_string;
  err = display_mode->GetName(&display_mode_string);
  if (err != S_OK)
  {
    return "";
  }
  std::string res = display_mode_string;
  free(reinterpret_cast<void*>(const_cast<char*>(display_mode_string)));
  return res;
}

void print_attributes(IDeckLink* card, IDeckLinkProfileAttributes* attributes,
  bool show_connector_attributes)
{
  HRESULT err;
  {
    int64_t interface_type;
    err = attributes->GetInt(BMDDeckLinkDeviceInterface, &interface_type);
    if (err == S_OK)
    {
      switch (interface_type)
      {
      case bmdDeviceInterfacePCI:
        std::cout << "PCIe\n";
        break;
      case bmdDeviceInterfaceUSB:
        std::cout << "USB\n";
        break;
      case bmdDeviceInterfaceThunderbolt:
        std::cout << "Thunderbolt\n";
        break;
      }
    }
    else
    {
      std::cout << "unknown device interface: " << err << "\n";
    }
  }
  {
    int64_t id;
    err = attributes->GetInt(BMDDeckLinkPersistentID, &id);
    if (err == S_OK)
    {
      std::cout << "Device Persistent ID: 0x" << std::hex << id << "\n";
    }
  }
  {
    int64_t id;
    err = attributes->GetInt(BMDDeckLinkTopologicalID, &id);
    if (err == S_OK)
    {
      std::cout << "Device Topological ID: 0x" << std::hex << id << "\n";
    }
  }
  {
    int64_t num;
    err = attributes->GetInt(BMDDeckLinkNumberOfSubDevices, &num);
    if (err == S_OK)
    {
      std::cout << "Number of sub-devices: " << std::dec << num << "\n";
      if (num)
      {
        int64_t idx;
        err = attributes->GetInt(BMDDeckLinkSubDeviceIndex, &idx);
        if (err == S_OK)
        {
          std::cout << "Sub-device index: " << std::dec << idx << "\n";
        }
      }
    }
  }

  if (show_connector_attributes)
  {
    bool supported;
    err = attributes->GetFlag(BMDDeckLinkHasSerialPort, &supported);
    if (supported)
    {
      std::cout << "Serial port present\n";
      const char* name;
      err = attributes->GetString(BMDDeckLinkSerialPortDeviceName, &name);
      if(err == S_OK)
      {
        std::cout << "Serial port name: " << name << "\n";
        free(reinterpret_cast<void*>(const_cast<char*>(name)));
      }
    }
    int64_t audio_channels;
    err = attributes->GetInt(BMDDeckLinkMaximumAudioChannels, &audio_channels);
    if (err == S_OK)
    {
      std::cout << "Number of audio channels: " << std::dec << audio_channels
                << "\n";
    }
    err = attributes->GetFlag(BMDDeckLinkSupportsInputFormatDetection, &supported);
    if (err == S_OK)
    {
      if (supported)
      {
        std::cout << "Supports input mode detection\n";
      }
    }
  }
}

void print_input_mode(IDeckLinkInput* card, BMDVideoConnection connection,
  BMDSupportedVideoModeFlags flags, IDeckLinkDisplayMode* display_mode,
  const char* suffix)
{
  static const std::vector<std::pair<BMDPixelFormat, std::string>>
    pixel_formats = {
      {bmdFormat8BitYUV, "8-bit YUV"},
      {bmdFormat10BitYUV, "10-bit YUV"},
      {bmdFormat8BitARGB, "8-bit ARGB"},
      {bmdFormat8BitBGRA, "8-bit BGRA"},
      {bmdFormat10BitRGB, "10-bit RGB"},
      {bmdFormat12BitRGB, "12-bit RGB"},
      {bmdFormat12BitRGBLE, "12-bit RGBLE"},
      {bmdFormat10BitRGBXLE, "10-bit RGBXLE"},
      {bmdFormat10BitRGBX, "10-bit RGBX"},
    };

  for (auto& pixel_format : pixel_formats)
  {
    bool supported;
    if (card->DoesSupportVideoMode(connection, display_mode->GetDisplayMode(),
          pixel_format.first, flags, &supported) == S_OK &&
        supported)
    {
      auto width = display_mode->GetWidth();
      auto height = display_mode->GetHeight();
      BMDTimeValue frameRateDuration;
      BMDTimeScale frameRateScale;
      display_mode->GetFrameRate(&frameRateDuration, &frameRateScale);
      std::cout << "    " << mode_name(display_mode) << suffix << ": " << width
                << "x" << height << " "
                << static_cast<double>(frameRateScale) /
                     static_cast<double>(frameRateDuration)
                << " " << pixel_format.second << "\n";
    }
  }
}

void print_input_modes_for_setup(IDeckLinkInput* input,
  BMDVideoConnection connection_type, BMDSupportedVideoModeFlags flags,
  const char* suffix)
{
  IDeckLinkDisplayModeIterator* iter = nullptr;
  IDeckLinkDisplayMode* display_mode = nullptr;
  HRESULT err;

  err = input->GetDisplayModeIterator(&iter);
  if (err != S_OK)
  {
    std::cout << "Couldn't obtain the video input display mode iterator: "
              << err << "\n";
    return;
  }

  while (iter->Next(&display_mode) == S_OK)
  {
    print_input_mode(input, connection_type, flags, display_mode, suffix);
    display_mode->Release();
  }
  iter->Release();
}

void print_input_modes(IDeckLink* card)
{
  IDeckLinkInput* input = nullptr;
  IDeckLinkProfileAttributes* attributes = nullptr;
  HRESULT err;
  int64_t ports;

  err =
    card->QueryInterface(IID_IDeckLinkInput, reinterpret_cast<void**>(&input));
  if (err != S_OK)
  {
    std::cerr << "could not obtain the IDeckLinkInput interface: " << err
              << "\n";
    goto bail;
  }

  err = card->QueryInterface(
    IID_IDeckLinkProfileAttributes, reinterpret_cast<void**>(&attributes));
  if (err != S_OK)
  {
    std::cerr << "Could not obtain the IDeckLinkProfileAttributes: " << err
              << "\n";
    goto bail;
  }

  attributes->GetInt(BMDDeckLinkVideoInputConnections, &ports);
  if (err != S_OK)
  {
    goto bail;
  }

  static const std::vector<std::pair<BMDVideoConnection, std::string>>
    connections = {
      {bmdVideoConnectionSDI, "SDI"},
      {bmdVideoConnectionHDMI, "HDMI"},
      {bmdVideoConnectionOpticalSDI, "Optical SDI"},
      {bmdVideoConnectionComponent, "Component"},
      {bmdVideoConnectionComposite, "Composite"},
      {bmdVideoConnectionSVideo, "S-Video"},
    };

  for (auto& connection : connections)
  {
    if (ports & connection.first)
    {
      std::cout << "  " << connection.second << ":\n";
      print_input_modes_for_setup(
        input, connection.first, bmdSupportedVideoModeDefault, "");
      std::cout << "\n";
      print_input_modes_for_setup(
        input, connection.first, bmdSupportedVideoModeDualStream3D, "3D");
      std::cout << "\n";
    }
  }

bail:
  if (attributes)
  {
    attributes->Release();
  }
  if (input)
  {
    input->Release();
  }
}

int main(int argc, char** argv)
{
  IDeckLinkIterator* iter = CreateDeckLinkIteratorInstance();
  HRESULT err;
  if (iter == nullptr)
  {
    std::cout << "missing decklink drivers?\n";
    return 1;
  }
  IDeckLinkAPIInformation* api_info;
  err = iter->QueryInterface(
    IID_IDeckLinkAPIInformation, reinterpret_cast<void**>(&api_info));
  if (err == S_OK)
  {
    // print out API information
    int64_t deckLinkVersion;

    // We can also use the BMDDeckLinkAPIVersion flag with GetString
    api_info->GetInt(BMDDeckLinkAPIVersion, &deckLinkVersion);

    int ver_major = (deckLinkVersion & 0xFF000000) >> 24;
    int ver_minor = (deckLinkVersion & 0x00FF0000) >> 16;
    int ver_patch = (deckLinkVersion & 0x0000FF00) >> 8;

    std::cout << "DeckLink API version: " << ver_major << "." << ver_minor
              << "." << ver_patch << "\n";
    api_info->Release();
  }

  // enumerate all cards
  IDeckLink* card;
  while (iter->Next(&card) == S_OK)
  {
    // print card name
    const char* name;
    err = card->GetModelName(&name);
    if (err == S_OK)
    {
      std::cout << "name: " << name << "\n";
      free(reinterpret_cast<void*>(const_cast<char*>(name)));
    }

    // get profile attributes (what are these?)
    bool show_io_info = true;
    IDeckLinkProfileAttributes* attributes;
    err = card->QueryInterface(
      IID_IDeckLinkProfileAttributes, reinterpret_cast<void**>(&attributes));
    if (err != S_OK)
    {
      std::cerr << "couldn't get IDeckLinkProfileAttributes: " << err << "\n";
      card->Release();
      continue;
    }

    int64_t duplex_mode;
    if (attributes->GetInt(BMDDeckLinkDuplex, &duplex_mode) == S_OK &&
        duplex_mode == bmdDuplexInactive)
    {
      std::cout << "sub-device has no active connectors for current profile\n";
      show_io_info = false;
    }

    // print attributes
    print_attributes(card, attributes, show_io_info);
    attributes->Release();

    if (show_io_info)
    {
      // list supported input modes
      print_input_modes(card);
    }

    card->Release();
  }
  iter->Release();
}
